import { Html, Head, Main, NextScript } from "next/document"
import Script from "next/script"

const GA_MEASUREMENT_ID = "UA-19531797-1"

export default function Document() {
  return (
    <Html lang="en">
      {/* <!-- Global site tag (gtag.js) - Google Analytics --> */}
      {/* https://nextjs.org/docs/messages/next-script-for-ga */}
      <Head>
        <Script
          src={`https://www.googletagmanager.com/gtag/js?id=${GA_MEASUREMENT_ID}`}
          strategy="afterInteractive"
        />
        <Script id="google-analytics" strategy="afterInteractive">
          {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', '${GA_MEASUREMENT_ID}');
        `}
        </Script>

        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          rel="preload"
          as="style"
          href="https://fonts.googleapis.com/css?family=PT+Serif&display=swap"
        />
        <link
          href="https://fonts.googleapis.com/css?family=PT+Serif&display=swap"
          rel="stylesheet"
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
