import "@/styles/app.scss"
import "@/styles/responsive.scss"
import type { AppProps } from "next/app"

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
