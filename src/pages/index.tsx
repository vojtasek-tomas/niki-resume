import getConfig from "next/config"
import Head from "next/head"
import { useEffect } from "react"
import { App } from "../components/App"

export default function Home() {
  useEffect(() => {
    const { hostname } = new URL(window.location.href)
    if (hostname !== "localhost" && hostname !== "vojtaskova.com") {
      window.location.href = "https://vojtaskova.com"
    }
  }, [])

  return (
    <>
      <Head>
        <title>Nikola Vojtášková - Gynekologická fyzioterapeutka</title>
        <meta
          name="description"
          content="Nikola Vojtášková - Fyzioterapeutka se zaměřením na ženskou problematiku"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <App />
    </>
  )
}
