import Image from "next/image"
import React from "react"
import Image1 from "../../public/images/gallery/1.jpg"
import Image3 from "../../public/images/gallery/3.jpg"
import Image4 from "../../public/images/gallery/4.jpg"
import Image5 from "../../public/images/gallery/5.jpg"
import Image6 from "../../public/images/gallery/6.jpg"
import Image7 from "../../public/images/gallery/7.jpg"

import IMG10 from "../../public/images/gallery/IMG_00010.jpg"
import IMG13 from "../../public/images/gallery/IMG_00013.jpg"
import IMG15 from "../../public/images/gallery/IMG_00015.jpg"

const images = [
  {
    src: IMG10,
    alt: "",
  },
  {
    src: IMG13,
    alt: "",
  },
  {
    src: IMG15,
    alt: "",
  },
  {
    src: Image6,
    alt: "",
  },
  {
    src: Image1,
    alt: "Tanec bez hranic 2018 - Pop Balet",
  },
  {
    src: Image3,
    alt: "Mistrovství světa ve florbale 2018 - muži",
  },
  {
    src: Image4,
    alt: "Mistrovství světa ve florbale 2018 - muži",
  },
  {
    src: Image5,
    alt: "Mistrovství světa ve florbale 2018 - muži",
  },
]

function Mansonry() {
  return (
    <div className="gallery">
      {images.map((image, i) => (
        <div className="unset-img" key={i}>
          <Image
            src={image.src}
            fill
            alt={image.alt}
            className="custom-img"
            priority={false}
          />
        </div>
      ))}
    </div>
  )
}

export function Gallery() {
  return (
    <section className="dark-section section">
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="heading">
              <h3>
                <b>Fotogalerie</b>
              </h3>
            </div>
            <Mansonry />
          </div>
        </div>
      </div>
    </section>
  )
}
