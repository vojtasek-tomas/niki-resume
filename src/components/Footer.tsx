import React from "react"

export function Footer() {
  return (
    <footer>
      <p className="copyright">
        Copyright &copy; {new Date().getFullYear()} All rights reserved | Based
        on template by{" "}
        <a href="https://colorlib.com" target="_blank" rel="noreferrer">
          Colorlib
        </a>
      </p>
    </footer>
  )
}
