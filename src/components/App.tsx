import React, { useState } from "react"
import { Header } from "./Header"
import { Section } from "./Section"
import { Gallery } from "./Gallery"
import { Footer } from "./Footer"
import { OtherInfo } from "./OtherInfo"

const educations = [
  {
    title: "Univerzita Karlova - Fakulta tělesné výchovy a sportu",
    subtitle:
      "navazující magisterské studium - obor fyzioterapie (zakončeno titulem Mgr.)",
    period: "2016 - 2019",
    text: "Téma diplomové práce: Dysmenorea a možnosti fyzioterapie",
  },
  {
    title: "Univerzita Karlova - 2. Lékařská fakulta",
    subtitle: "bakalářské studium - obor fyzioterapie (zakončeno titulem Bc.)",
    period: "2012 - 2015",
    text: "Téma bakalářské práce: Možnosti fyzioterapie u pacientů s ALS s důrazem na respirační problematiku",
  },
]

const degrees = [
  "Bc.",
  "BcA.",
  "Ing.",
  "Ing. arch.",
  "ICDr.",
  "JUDr.",
  "MDDr.",
  "Mgr.",
  "MgA.",
  "MSDr.",
  "MUDr.",
  "MVDr.",
  "PaedDr.",
  "PhDr.",
  "PhMr.",
  "RCDr.",
  "RNDr.",
  "RSDr.",
  "RTDr.",
  "ThDr.",
  "ThLic.",
  "ThMgr.",
  "akad.",
  "ArtD.",
  "CSc.",
  "Dr.",
  "DrSc.",
  "DSc.",
  "Ph.Dd.",
  "Th.Dd.",
  "as.",
  "odb. as.",
  "doc.",
  "prof.",
].join("|")

const courseYears = [
  {
    date: "2024",
    courses: [
      "Odborný kurz v Rehaeduca - Evidence based přístup k jizvám, adhezím a viscerálním srůstům",
    ],
  },
  {
    date: "2024",
    courses: [
      "Be Balanced - Mgr. Michaela Havlíčková - Kurz terapie v těhotenství a po porodu",
    ],
  },
  {
    date: "2023",
    courses: [
      "Rehabilitace Bitnar - Viscerovertebrální vztahy a jejich využití v klinické praxi se zaměřením na GERD",
    ],
  },
  {
    date: "2021",
    courses: [
      "Be Balanced - Mgr. Michaela Havlíčková - Kurz terapie u dysfunkcí pánevního dna",
    ],
  },
  {
    date: "2020",
    courses: [
      "Odborný online kurz v CKP - ploska, helezenní kloub - Diagnostika a cesta k pohybové efektivitě",
      "Fyzioterapie Vlasta Bezvodová - Certifikovaný kurz Rehabilitační léčba některých druhů ženské sterility metodou Ludmily Mojžíšové",
    ],
  },
  {
    date: "2019",
    courses: [
      "Instruct - Podiatrie - Fyzioterapie nohy",
      "MedFeet s. r. o. - Praktická podiatrie s konceptem Vasyli Medical",
    ],
  },
  {
    date: "2018",
    courses: [
      "Nemocnice na Homolce - stáž na oddělení RFM - vyšetření a ošetření pánevního dna pomocí přístrojů Gymna 420 a Gymna 200; práce s pacienty trpící problematikou pánevního dna",
    ],
  },
  {
    date: "2017",
    courses: [
      "Monáda - certifikovaný kurz Fyzioterapie dětí ve věku od 2 do 15 let",
      "FC Rehamil - Certifikovaný kurz: Rehabilitační léčba některých druhů funkční ženské sterility metodou L. Mojžíšové; část B: Ošetření svaloviny pánevního dna metodou L. Mojžíšové",
      "Odborný kurz u PhDr. Petra Šifty, Ph.D. - Speciální kineziologie pohybového aparátu člověka - diferencionální diagnostika a návrh terapie",
      "Odborný kurz v HT institut - Reflexní terapie 1",
    ],
  },
  {
    date: "2016",
    courses: [
      "Odborný kurz v Rehaeduca - Manuální medicína u gynekologických dysfunkcí a v těhotenství",
      "Odborný kurz v CKP Dobřichovice - Vztah mezi dechovými pohyby a posturou",
      "Školící akce v CKP Sámova - Gynekologie pro fyzioterapeuty",
      "Školící akce v CKP Dobřichovice - Workshop Feldenkraisovy metody pro začátečníky a mírně pokročilé",
    ],
  },
  {
    date: "2015",
    courses: [
      "Odborný kurz v CKP Dobřichovice - Diagnostika a kinezioterapie u idiopatické skoliózy u dětí a dospělých",
      "Odborný kurz v EduSpa college - kurz kineziotapingu",
    ],
  },
  {
    date: "2013",
    courses: [
      "CKP Dobřichovice - Komplexní rehabilitační lékařství a psychosomatika, lekt. MUDr. J.Hnízdil",
    ],
  },
]

const previousWork = [
  {
    title: "CENTRUM FYZIO KOMPLEX",
    position: "Fyzioterapeut",
    period: "únor 2019 - srpen 2019",
  },
  {
    title: "Mistrovství světa ve florbale 2018 - muži",
    position: "Fyzioterapeut australského týmu",
    period: "listopad - prosinec 2018",
  },
  {
    title: "Institut sportovního lékařství",
    position: "Fyzioterapeut",
    period: "listopad 2016 - prosinec 2018",
  },
  {
    title: "FK Viktoria Žižkov",
    position: "Fyzioterapeut dětí ve věku od 5 let",
    period: "listopad 2017 - prosinec 2018",
  },
  {
    title: "Rugby club Sparta Praha",
    position: "Fyzioterapeut",
    period: "duben 2017 - červen 2017",
  },
  {
    title: "Rehabilitace Dejvice s.r.o.",
    position: "Fyzioterapeut",
    period: "říjen 2015 - říjen 2016",
  },
]

const individualClients = [
  {
    title: "Pop Balet",
    position: "Fyzioterapeut / zdravotník",
    period: "srpen 2018",
    description:
      "Zdravotník a fyzioterapeut pro děti a dospělé na mezinárodním tanečním kempu.",
  },
  {
    title: "Radko Gudas - hokejista NHL",
    position: "Fyzioterapeut",
    period:
      "květen - srpen 2016, červen - srpen 2017, červenec - srpen 2018, červenec - srpen 2019",
    description:
      "Techniky měkkých tkání, mobilizace, LTV na NFP, analytické LTV, kinesiotaping, komplexní poradenství",
  },
  {
    title: "Daniel Vašák - pacient s ALS",
    position: "Fyzioterapeut",
    period: "červen 2014 - říjen 2015",
    description: "Komplexní terapie u pacienta s ALS",
  },
]

const degreeRegexp = new RegExp(`(${degrees}) `, "g")

// https://github.com/pavelglac/Automaticka-uprava-typografie/blob/master/autocorrector.js
// https://github.com/felixprojekt/nbsp2preposition/blob/master/nbsp-script.php

const sanitize = (str: string) =>
  str
    .replace(/-/g, "–") // en-dash
    .replace(/([A-Z])\. ([A-Z])/g, "$1. $2") // L. Mojžíšové
    .replace(/(a|i|o|u|s|z|k|v|A|I|O|U|S|Z|K|V) /g, "$1 ")
    .replace(degreeRegexp, "$1 ") // Mudr. J. Hnízdil

export function App() {
  const SLICE = 4
  const mainCourseYears = courseYears.slice(0, SLICE)
  const otherCourseYears = courseYears.slice(SLICE)
  const [showMore, setShowMore] = useState(false)

  const renderCourses = (courses: typeof courseYears) => {
    return (
      <>
        {courses.map((year, i) => (
          <React.Fragment key={i}>
            <h4 className="font-yellow">
              <b>{sanitize(year.date)}</b>
            </h4>
            <ul className="list">
              {year.courses.map((course, i) => (
                <li key={i} className="font-semi-white">
                  {sanitize(course)}
                </li>
              ))}
            </ul>
          </React.Fragment>
        ))}
      </>
    )
  }

  return (
    <>
      <Header />
      <Section variant="dark" title="Současná zaměstnání">
        <h4>
          <b>VO2MAX</b>
        </h4>
        <h5 className="font-yellow">
          <b>Fyzioterapeut</b>
        </h5>
        <h6 className="mt-10">od září 2019</h6>
        <p className="font-semi-white my-30">
          Ambulantní práce s pacienty, gynekologická fyzioterapie, techniky
          měkkých tkání, mobilizace, LTV na NFP, analytické LTV, kinesiotaping,
          obsluha přístrojů fyzikální terapie
        </p>
      </Section>

      <Section variant="light" title="Vzdělání">
        <div className="education-wrapper">
          {educations.map((education, i) => (
            <div key={i} className="education mb-50">
              <h4>
                <b>{sanitize(education.title)}</b>
              </h4>
              <h5 className="font-yellow">
                <b>{sanitize(education.subtitle)}</b>
              </h5>
              <h6 className="font-lite-black mt-10">
                {sanitize(education.period)}
              </h6>
              <p className="my-30">{sanitize(education.text)}</p>
            </div>
          ))}
        </div>
      </Section>

      <Section variant="dark" title="Další vzdělání / kurzy">
        {renderCourses(mainCourseYears)}

        <div style={{ height: showMore ? "auto" : 0, overflow: "hidden" }}>
          {renderCourses(otherCourseYears)}
        </div>
        <button
          className="show-more-button"
          onClick={(event) => {
            setShowMore((prev) => !prev)
            const target = event.currentTarget
            if (showMore) {
              window.requestAnimationFrame(() => {
                target.scrollIntoView({
                  behavior: "instant",
                  block: "center",
                })
              })
            }
          }}
        >
          Zobrazit {showMore ? "méně" : "více"}
        </button>
      </Section>

      <Section variant="light" title="Odbornost">
        <p className="font-yellowfont-semi-white mb-30">
          Osvědčení k výkonu zdravotnického povolání bez odborného dohledu
          v oboru fyzioterapeut.
        </p>
      </Section>

      <Gallery />

      <Section variant="light" title="Předchozí zaměstnání">
        {previousWork.map((work, i) => (
          <div key={i} className="experience mb-50">
            <h4>
              <b>{sanitize(work.title)}</b>
            </h4>
            <h5 className="font-yellow">
              <b>{sanitize(work.position)}</b>
            </h5>
            <h6 className="mt-10">{sanitize(work.period)}</h6>
          </div>
        ))}
      </Section>

      <Section variant="dark" title="Aktivní činnost">
      <div className="experience mb-50">
          <h4>
            <b>Konference Tělovýchovné lékařství 2024</b>
          </h4>
          {/* <h5 className="font-yellow">
            <b>Poruchy funkce pánevního dna u sportujících žen pohledem gynekologické fyzioterapeutky</b>
          </h5> */}
          <h6 className="mt-10">
            {sanitize("7. až 9. listopadu 2024 - Ostrava")}
          </h6>
          <p className="mt-10 font-semi-white">
            Autorka přednášky:
            <br />
            "Poruchy funkce pánevního dna u sportujících žen pohledem gynekologické fyzioterapeutky"
          </p>
        </div>

        <div className="experience mb-50">
          <h4>
            <b>Konference CEREBROCON 2018</b>
          </h4>
          <h5 className="font-yellow">
            <b>Amyotrofická laterální skleróza (ALS)</b>
          </h5>
          <h6 className="mt-10">
            {sanitize("10. a 11. října 2018 - Jánské Lázně")}
          </h6>
          <p className="mt-10 font-semi-white">
            Autorka přednášky:
            <br />
            "Respirační problematika pacientů s ALS a její možnosti
            fyzioterapie"
          </p>
        </div>
      </Section>

      <Section variant="light" title="Individuální klienti">
        {individualClients.map((client, i) => (
          <div key={i} className="experience mb-50">
            <h4>
              <b>{sanitize(client.title)}</b>
            </h4>
            <h5 className="font-yellow">
              <b>{sanitize(client.position)}</b>
            </h5>
            <h6 className="mt-10">{sanitize(client.period)}</h6>
            <p className="my-30">{sanitize(client.description)}</p>
          </div>
        ))}
      </Section>

      <OtherInfo />
      <Footer />
    </>
  )
}
