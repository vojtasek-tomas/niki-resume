import { useState, useEffect } from "react"

import ProfileJPG from "../../public/images/profile.jpg"
import LogoLinkedin from "../../public/icons/linkedin.svg"

export function Header() {
  const [email, setEmail] = useState<string>("")

  useEffect(() => {
    const timeoutId = setTimeout(
      () => setEmail("niki.vojtaskova@gmail.com"),
      100
    )

    return () => {
      clearTimeout(timeoutId)
    }
  }, [])
  return (
    <section className="intro-section">
      <div className="container">
        <div className="row justify-center">
          <div className="intro">
            <div className="profile-img">
              <picture>
                <source srcSet={ProfileJPG.src} type="image/jpeg" />
                <img src={ProfileJPG.src} alt="Nikola Vojtášková" />
              </picture>
            </div>
            <h2>
              <b>Mgr. Nikola Vojtášková</b>
            </h2>
            <h4 className="font-yellow">Gynekologická fyzioterapeutka</h4>
            <ul className="information my-30">
              <li>Praha</li>
              {email && (
                <li>
                  <a href={`mailto:${email}`}>{email}</a>
                </li>
              )}
            </ul>
            <ul className="social-icons">
              <li>
                <a href="https://www.linkedin.com/in/nikola-vojt%C3%A1%C5%A1kov%C3%A1-09147b166/">
                  <LogoLinkedin style={{ fontSize: 20 }} color="white" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  )
}
