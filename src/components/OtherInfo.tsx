import React from "react"

export function OtherInfo() {
  return (
    <section className="iconboxes-section dark-section section">
      <div className="container">
        <div className="row">
          <div className="col-sm-6">
            <div className="heading">
              <h4>
                <b>Jazykové znalosti</b>
              </h4>
            </div>
            <div className="iconbox mb-30">
              <ul>
                <li>
                  <b>Angličtina</b> – aktivně
                </li>
                <li>
                  <b>Francouzština</b> – středně pokročilá znalost
                </li>
                <li>
                  <b>Polština</b> – aktivně
                </li>
                <li>
                  <b>Ruština</b> – začátečník
                </li>
              </ul>
            </div>
          </div>

          <div className="col-sm-6">
            <div className="heading">
              <h4>
                <b>Zájmy</b>
              </h4>
            </div>
            <div className="iconbox mb-30">
              Ve volném čase se věnuji
              rodině. Díky osobním zážitkům dokáži lépe porozumět jiným
              zvykům a kulturám. Velmi důležitým koníčkem je pro mě sport, který
              beru jako formu relaxu. Mezi mé další zájmy patří například tanec,
              kterému jsem se věnovala závodně 14 let a čtení nejen beletrie,
              ale i odborné literatury.
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
