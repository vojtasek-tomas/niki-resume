import React, { ReactNode } from "react"

type Props = {
  variant?: "dark" | "light"
  title: string
  children: ReactNode
}

export function Section(props: Props) {
  const { variant, title } = props
  const className = `${variant || "dark"}-section section`
  return (
    <section className={className}>
      <div className="container">
        <div className="row">
          <div className="col-sm-4">
            <div className="heading">
              <h3>
                <b>{title}</b>
              </h3>
            </div>
          </div>
          <div className="col-sm-8">
            <div className="experience">{props.children}</div>
          </div>
        </div>
      </div>
    </section>
  )
}
